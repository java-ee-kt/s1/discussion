/**
 * WebMathServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sample.ws;

public class WebMathServiceLocator extends org.apache.axis.client.Service implements com.sample.ws.WebMathService {

    public WebMathServiceLocator() {
    }


    public WebMathServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WebMathServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WebMath
    private java.lang.String WebMath_address = "http://localhost:8089/WebMath/services/WebMath";

    public java.lang.String getWebMathAddress() {
        return WebMath_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WebMathWSDDServiceName = "WebMath";

    public java.lang.String getWebMathWSDDServiceName() {
        return WebMathWSDDServiceName;
    }

    public void setWebMathWSDDServiceName(java.lang.String name) {
        WebMathWSDDServiceName = name;
    }

    public com.sample.ws.WebMath getWebMath() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WebMath_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWebMath(endpoint);
    }

    public com.sample.ws.WebMath getWebMath(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            com.sample.ws.WebMathSoapBindingStub _stub = new com.sample.ws.WebMathSoapBindingStub(portAddress, this);
            _stub.setPortName(getWebMathWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWebMathEndpointAddress(java.lang.String address) {
        WebMath_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (com.sample.ws.WebMath.class.isAssignableFrom(serviceEndpointInterface)) {
                com.sample.ws.WebMathSoapBindingStub _stub = new com.sample.ws.WebMathSoapBindingStub(new java.net.URL(WebMath_address), this);
                _stub.setPortName(getWebMathWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WebMath".equals(inputPortName)) {
            return getWebMath();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws.sample.com", "WebMathService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws.sample.com", "WebMath"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WebMath".equals(portName)) {
            setWebMathEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
