package com.sample.ws;

public class WebMathProxy implements com.sample.ws.WebMath {
  private String _endpoint = null;
  private com.sample.ws.WebMath webMath = null;
  
  public WebMathProxy() {
    _initWebMathProxy();
  }
  
  public WebMathProxy(String endpoint) {
    _endpoint = endpoint;
    _initWebMathProxy();
  }
  
  private void _initWebMathProxy() {
    try {
      webMath = (new com.sample.ws.WebMathServiceLocator()).getWebMath();
      if (webMath != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)webMath)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)webMath)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (webMath != null)
      ((javax.xml.rpc.Stub)webMath)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public com.sample.ws.WebMath getWebMath() {
    if (webMath == null)
      _initWebMathProxy();
    return webMath;
  }
  
  public int addition(int num1, int num2) throws java.rmi.RemoteException{
    if (webMath == null)
      _initWebMathProxy();
    return webMath.addition(num1, num2);
  }
  
  
}