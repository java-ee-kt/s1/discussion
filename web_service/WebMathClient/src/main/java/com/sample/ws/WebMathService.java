/**
 * WebMathService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.sample.ws;

public interface WebMathService extends javax.xml.rpc.Service {
    public java.lang.String getWebMathAddress();

    public com.sample.ws.WebMath getWebMath() throws javax.xml.rpc.ServiceException;

    public com.sample.ws.WebMath getWebMath(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
