package com.sample.MicroserviceSample.HelloWorld;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController  
public class HelloWorldRestController {

	@RequestMapping("/")
	public String hello(){
		return "<h1>Hello World!</h1>";
	}

	//Mini-Activity
	//Create a new route by adding the @RequestMapping annotation
	//to assign an endpoint ("/myName")
	//Then create a method called getName()
	//this method should be able to return your full name as an h1 element.

	@RequestMapping("/myName")
	public String getName(){
		return "<h1>I am George Miller</h1>";
	}

	//Mini-Activity
	//Create a new route by adding the @RequestMapping annotation
	//to assign an endpoint ("/myDescription")
	//Then create a method called getDescription()
	//this method should be able to return a short description about you as a paragraph element.

	@RequestMapping("/myDescription")
	public String getDescription(){
		return "<p>I am George Miller. I live in Sydney, Australia. I am a web developer. I have been working as a programmer for 5 years.</p>";
	}

	//Mini-Activity
	//Create a new route by adding the @RequestMapping annotation
	//to assign an endpoint ("/myFavoriteMovies")
	//Then create a method called getFavoriteMovies()
	//this method should be able to return a short unordered list of your favorite movies.

	@RequestMapping("/myFavoriteMovies")
	public String getFavoriteMovies(){
		return "<h2>My Favorite Movies</h2>" +
				"<ul>" +
				"<li>The Godfather</li>" +
				"<li>Schindler's List</li>" +
				"<li>Shawshank Redemption</li>" +
				"<li>Meet the Robinsons</li>" +
				"<li>Dune</li>" +
				"</ul>";
	}
}