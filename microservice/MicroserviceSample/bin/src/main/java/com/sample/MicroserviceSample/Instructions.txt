
How to create a simple microservice using springboot framework (Eclipse)

1. go to https://start.spring.io/ to create a spring initializr
		- Project: Maven
		- Language: Java
		- Spring Boot : 2.6
		- Packaging : JAR
		- Java: 11
		- Project Metadata
			- input desired group name, artifact name, and description
		
		-Dependencies (Add desired dependencies, for this sample add the following)
			-  Spring Web
			-  Lombok
			
2. Press Generate button to download JAR zip file
		- unzip on desired location

3. Open Eclipse >File > Import...> Maven
		> Existing Maven Projects> Next
		
4. At the maven projects modal> click browse> look for unzipped Spring Boot Jar Folder
		>Open> Finish
		- Dowloaded Springboot app will appear on Project explorer

5. To test if Spring Boot app is working > go to src/main/java folder> package
		> right click <app name>application.java> run as> java application
		- app should be running in console without errors and running at port 8080

6. On project explorer right click on the package above the spring boot application .java (file with main())
		>New> package> add .<package name> to the parent pacakge name> Finish
			- e.g. com.sample.MicroserviceSample.HelloWorld

7. Create a new class under the package you just created (for this sample I named it HelloWorldRestController)
		- copy the following codes:
		
		@RestController  
		public class HelloWorldRestController {

			@RequestMapping("/")  
			public String hello(){  
				return "<h1>Hello World</h1>";  
			}  
		}
		
		// note that @RestController and @RequestMapping will be auto imported, if not,
			type on top of the  class :
				import org.springframework.web.bind.annotation.RequestMapping;
				import org.springframework.web.bind.annotation.RestController; 
	
8. To run microservice, Go back and right click the application.java (where main is)> run as > java application
				
9. microservice will initialize in console> go to browser> localhost:8080
		Expected output in browser:
			"Hello World"
			
			
			
			
			
			
			
			
			
		
			 


			
		

